/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"ns_CarSharingUI_Resp/CarSharingUI_Resp/test/integration/AllJourneys"
	], function () {
		QUnit.start();
	});
});