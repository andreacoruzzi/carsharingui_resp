/*global QUnit*/

sap.ui.define([
	"ns_CarSharingUI_Resp/CarSharingUI_Resp/controller/manager.controller"
], function (Controller) {
	"use strict";

	QUnit.module("manager Controller");

	QUnit.test("I should test the manager controller", function (assert) {
		var oAppController = new Controller();
		oAppController.onInit();
		assert.ok(oAppController);
	});

});